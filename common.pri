PROJECT_ROOT_PATH = $${PWD}

CONFIG(debug, debug|release) {
    BUILD_FLAG = debug
    LIB_SUFFIX = d
} else {
    BUILD_FLAG = release
}

LIBS_PATH = $${PROJECT_ROOT_PATH}/lib
INC_PATH = $${PROJECT_ROOT_PATH}/include
IMPORT_PATH = $${PROJECT_ROOT_PATH}/import
BIN_PATH = $${PROJECT_ROOT_PATH}/bin/$${BUILD_FLAG}

BUILD_PATH = $${PROJECT_ROOT_PATH}/build/$${BUILD_FLAG}/$${TARGET}/
RCC_DIR = $${BUILD_PATH}/rcc/
UI_DIR = $${BUILD_PATH}/ui/
MOC_DIR = $${BUILD_PATH}/moc/
OBJECTS_DIR = $${BUILD_PATH}/obj/

linux-g++: QMAKE_CXXFLAGS += -std=c++14

INCLUDEPATH += $${PROJECT_ROOT_PATH}/include/
LIBS += -L$${LIBS_PATH}

HEADERS += \
    $$PWD/include/crypt_key.hpp
