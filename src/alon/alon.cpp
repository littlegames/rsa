#include <alon.hpp>

#include <cmath>
#include <cstdlib>
#include <sstream>

class math_err {
    const char *m_what;
public:
    const char *what() { return m_what; }
    math_err(const char *input) : m_what (input) {}
};

alon::alon()
    : m_currentSize (0)
    , m_maxSize     (10)
    , m_positive    (true)
{
    init();
}

alon::alon(unsigned size)
    : m_currentSize (0)
    , m_maxSize     (size)
    , m_positive    (true)
{
    init();
}

alon::alon(const alon &obj)
    : m_currentSize (obj.m_currentSize)
    , m_maxSize     (obj.m_maxSize)
    , m_positive    (obj.m_positive)
{
    init(obj.m_number, m_maxSize);
}

alon::~alon()
{
    delete [] m_number;
}

std::vector<char> alon::toBinary()
{
    constexpr unsigned short ARRAY_SIZE = 4;
    std::vector<char> r;
    std::vector<char> tmp;
    alon q = *this;
    alon reminder;
    alon hex; hex = 16;
    short  hexVal = 0;

    while (q != 0) {
        q = q.div_mod(hex, reminder);

        if (reminder.m_currentSize == 1) {
            hexVal = reminder.m_number[0];
        } else {
            hexVal = (reminder.m_currentSize == 2)
                   ? reminder.m_number[0] + reminder.m_number[1] * 10
                   : 0;
        }

        for (unsigned i = 0; i < ARRAY_SIZE; i += 1) {
            char buff = (hexVal & 1) ? 1 : 0;
            tmp.push_back(buff);
            hexVal = hexVal >> 1;
        }
    }

    bool first = true;
    for (int i = tmp.size() - 1; i >= 0; i -= 1) {
        if (tmp[i] == 1)
            first = false;
        if (!first)
            r.push_back(tmp[i]);
    }

    return r;
}

unsigned alon::size()
{
    return m_currentSize;
}

alon alon::pow(const unsigned power)
{
    alon r;

    if (power == 0) {
        r = 1;
        return r;
    }

    r = *this;
    alon pw; pw = power;
    std::vector<char> d = pw.toBinary();

    for (unsigned i = 1; i < d.size(); i += 1) {
        r = (d[i])
            ? (r * r * *this)
            : (r * r);
    }

    r.m_positive = (pw.even()) ? true : m_positive;

    return r;
}

alon alon::abs()
{
    alon r(*this);
    r.m_positive = true;
    return r;
}

void alon::concat(const alon &obj)
{
    reflect();

    unsigned j = m_currentSize;
    for (int i = obj.m_currentSize - 1; i >= 0; i -= 1) {
        m_number[j] = obj.m_number[i];
        m_currentSize += 1;
        j += 1;
    }

    reflect();
}

bool alon::even()
{
    short element = m_number[0];

    if (element & 1) return false;

    return true;
}

alon alon::pow_mod(const std::vector<char> &d, const alon &m)
{
    alon r = *this;

    for (unsigned i = 1; i < d.size(); i += 1) {
        r = (d[i])
          ? (r * r * *this) % m
          : (r * r) % m;
    }

    return r;
}

bool alon::prime()
{
    if (*this == 1 || m_currentSize == 0)   return false;
    if (*this == 2 || *this == 3)           return true;
    if (even() )                            return false;

    alon tmp(*this);
    alon x;
    alon a(m_currentSize);
    alon t(tmp);
    tmp -= 1;
    unsigned s = 0;

    do {
        t = t / 2;
        s += 1;
    } while (t.even());

    std::vector<char> bin_t;
    bin_t = t.toBinary();

    std::vector<char> bin_two(2);
    bin_two[0] = 1;
    bin_two[1] = 0;

    unsigned i = 0;

    while (i < m_currentSize) {
start:

        if (i == m_currentSize - 1)
            break;

        i += 1;

        if (m_currentSize > 3) {
            int randomSize = rand() % (m_currentSize - 3) + 2;
            a.random(randomSize);
        } else {
            do {
                a.random(1);
            } while (!(a > 1 && a < tmp) );
        }

        x = a.pow_mod(bin_t, *this); // x = a ^ (t mod *this)

        if ((x == 1) || (x == tmp) )
            continue;

        for (unsigned j = 0; j < s - 1; j += 1) {
            x = x.pow_mod(bin_two, *this); // x = x ^ ('10' mod *this)

            if (x == 1)     return false;
            if (x == tmp)   goto start; //repeat while
        }

        return false;
    }

    return true;
}

void alon::random(unsigned lenght)
{
    if (lenght <= m_maxSize) {
        for (unsigned i = 0; i < lenght; i += 1)
            m_number[i] = (i != lenght - 1) ? rand() % 10 : rand() % 9 + 1;

        m_currentSize = lenght;
        m_positive = true;
    } else
        throw math_err("specified size exceeds the maximum capacity of the "
                      "object");
}

alon alon::max_divisor(alon b)
{
    alon a(*this);
    alon c;

    while (b != 0) {
        c = a % b;
        a = b;
        b = c;
    }

    return a.abs();
}

alon alon::operator =(const alon &right)
{
    delete [] m_number;

    m_maxSize     = right.m_maxSize;
    m_currentSize = right.m_currentSize;
    m_positive    = right.m_positive;

    init(right.m_number, m_currentSize);

    return *this;
}

alon alon::operator =(const int right)
{
    m_currentSize = 0;
    m_positive = (right < 0) ? false : true;
    std::string str;
    char tmp;

    if (right == 0) {
        init();
        return *this;
    }

    std::stringstream ss;
    ss << ::abs(right);
    ss >> str;

    for (unsigned i = 0; i < str.size(); i += 1) {
        if (i == m_maxSize)
            break;

        tmp = str[i];
        ss.clear();
        ss << tmp;
        ss >> m_number[i];
        m_currentSize += 1;
    }

    reflect();
    return *this;
}

alon alon::operator +(const alon &right)
{
    unsigned size = (m_maxSize >= right.m_maxSize)
             ? m_maxSize + 1
             : right.m_maxSize + 1;

    alon r(size);

    if (m_currentSize == 0 && right.m_currentSize == 0)
        return r;

    short cache = 0;
    short sum   = 0;
    short a     = 0;
    short b     = 0;

    unsigned maxpos = (m_currentSize >= right.m_currentSize)
                    ? m_currentSize - 1
                    : right.m_currentSize - 1;

    if ((m_positive && right.m_positive)
    || (!m_positive && !right.m_positive) ) {

        unsigned i;
        for (i = 0; i <= maxpos; i += 1) {
            sum   = cache;
            a     = (m_currentSize > i)       ? m_number[i]       : 0;
            b     = (right.m_currentSize > i) ? right.m_number[i] : 0;
            sum  += a + b;

            cache = short(sum / 10.0);
            sum  -= cache * 10;

            r.m_number[i] = sum;
            r.m_currentSize += 1;
        }

        if (cache) {
            r.m_number[i] = cache;
            r.m_currentSize += 1;
        }

        if (!m_positive && !right.m_positive)
            r.m_positive = false;

    } else if ((!m_positive && right.m_positive)
             || (m_positive && !right.m_positive) ) {

        if (!isAbs(right)) {
            for (unsigned i = 0; i <= maxpos; i += 1) {
                sum   = cache;
                cache = 0;
                a     = (m_currentSize > i)       ? m_number[i]       : 0;
                b     = (right.m_currentSize > i) ? right.m_number[i] : 0;

                sum  += a - b;
                if (a + sum < b) {
                    sum += 10;
                    cache = -1;
                }

                r.m_number[i] = sum;
                r.m_currentSize += 1;
            }

            if (!m_positive && right.m_positive)
                r.m_positive = false;

        } else {
            for (unsigned i = 0; i <= maxpos; i += 1) {
                sum   = cache;
                cache = 0;
                a     = (m_currentSize > i)       ? m_number[i]       : 0;
                b     = (right.m_currentSize > i) ? right.m_number[i] : 0;

                sum  += b - a;
                if (b + sum < a) {
                    sum += 10;
                    cache = -1;
                }

                r.m_number[i] = sum;
                r.m_currentSize += 1;
            }

            if (m_positive && !right.m_positive)
                r.m_positive = false;
        }
    }

    r.remove_space();
    return r;
}

alon alon::operator +(const int right)
{
    alon tmp;   tmp = right;
    alon r;     r   = tmp + *this;
    return r;
}

alon alon::operator *(const alon &right)
{
    alon tmp(m_currentSize + right.m_currentSize);
    alon r;

    int   shift   = 0;
    short product;
    short cache   = 0;

    for (unsigned i = 0; i < m_currentSize; i += 1) {
        tmp   = 0;
        cache = 0;

        for (unsigned j = 0; j < right.m_currentSize; j += 1) {
            product  = cache + m_number[i] * right.m_number[j];

            cache    = short(product / 10.0);
            product  -= cache * 10;

            tmp.m_number[j + shift] = product;
            tmp.m_currentSize += 1;
        }

        tmp.m_currentSize += shift;
        if (cache) {
            tmp.m_number[tmp.m_currentSize] = cache;
            tmp.m_currentSize += 1;
        }
        shift += 1;
        r += tmp;
    }

    if ((!m_positive && right.m_positive)
      || (m_positive && !right.m_positive) ) {
        r.m_positive = false;
    }

    return r;
}

alon alon::operator *(const int right)
{
    alon tmp;   tmp = right;
    alon r;     r   = *this * tmp;
    return r;
}

alon alon::operator -(const alon &right)
{
    alon r; r = *this + -right;
    return r;
}

alon alon::operator -(const int right)
{
    alon tmp;   tmp = right;
    alon r;     r   = *this - tmp;
    return r;
}

alon alon::operator /(const alon &right)
{
    if (right.m_currentSize == 0)
        throw math_err("division by zero");

    alon buf;
    alon r;     r = div_mod(right, buf);

    if ((!m_positive && right.m_positive)
      || (m_positive && !right.m_positive) ) {
        r.m_positive = false;
    }

    return r;
}

alon alon::operator /(const int right)
{
    if (right == 0)
        throw math_err("division by zero");

    alon tmp;   tmp = right;
    alon r;     r   = *this / tmp;

    return r;
}

alon alon::operator %(const alon &right)
{
    if (right.m_currentSize == 0)
        throw math_err("reminder of division by zero");

    alon r;
    div_mod(right, r);

    if (!m_positive)
        r.m_positive = false;

    return r;
}

alon alon::operator %(const int right)
{
    if (right == 0)
        throw math_err("reminder of division by zero");

    alon tmp;   tmp = right;
    alon r;     r   = *this % tmp;

    return r;
}

alon alon::operator +=(const alon &right)
{
    *this = *this + right;
    return *this;
}

alon alon::operator +=(const int right)
{
    *this = *this + right;
    return *this;
}

alon alon::operator -=(const alon &right)
{
    *this = *this - right;
    return *this;
}

alon alon::operator -=(const int right)
{
    *this = *this - right;
    return *this;
}

bool alon::operator >(const alon &right)
{
    if (m_positive && !right.m_positive) return true;
    if (!m_positive && right.m_positive) return false;

    if (m_positive && right.m_positive) {
        if (m_currentSize > right.m_currentSize) return true;
        if (m_currentSize < right.m_currentSize) return false;

        for (int i = m_currentSize - 1; i >= 0; i -= 1) {
            if (m_number[i] > right.m_number[i]) return true;   else
            if (m_number[i] < right.m_number[i]) return false;
        }
    }

    if (!m_positive && !right.m_positive) {
        if (m_currentSize > right.m_currentSize) return false;
        if (m_currentSize < right.m_currentSize) return true;

        for (int i = m_currentSize - 1; i >= 0; i -= 1) {
            if (m_number[i] > right.m_number[i]) return false;   else
            if (m_number[i] < right.m_number[i]) return true;
        }
    }

    return false;
}

bool alon::operator >(const int right)
{
    alon tmp;   tmp = right;

    if (*this > tmp)
        return true;

    return false;
}

bool alon::operator <(const alon &right)
{
    alon tmp(right);
    return (tmp > *this);
}

bool alon::operator <(const int right)
{
    alon tmp;   tmp = right;

    if (*this < tmp)
        return true;

    return false;
}

bool alon::operator ==(const alon &right)
{
    if ((!m_positive && right.m_positive)
      || (m_positive && !right.m_positive) ) {
        return false;
    }

    if (m_currentSize != right.m_currentSize)
        return false;

    for (unsigned i = 0; i < m_currentSize; i += 1)
        if (m_number[i] != right.m_number[i])
            return false;
    return true;
}

bool alon::operator ==(const int right)
{
    alon tmp;   tmp = right;
    return (*this == tmp);
}

bool alon::operator !=(const alon &right)
{
    return (!(*this == right));
}

bool alon::operator !=(const int right)
{
    alon tmp;   tmp = right;
    return (!(*this == tmp));
}

bool alon::operator >=(const alon &right)
{
    return (*this > right) || (*this == right);
}

bool alon::operator >=(const int right)
{
    alon tmp;   tmp = right;
    return (*this > tmp) || (*this == tmp);
}

bool alon::operator <=(const alon &right)
{
    return (*this < right) || (*this == right);
}

bool alon::operator <=(const int right)
{
    alon tmp;   tmp = right;
    return (tmp < right) || (tmp == right);
}

alon alon::div_mod(const alon &obj, alon &reminder)
{
    alon part;
    alon r(m_currentSize);
    alon tmp(obj.m_currentSize + 1);

    int      position = m_currentSize - 1;
    unsigned i        = 0;
    unsigned j        = 0;
    unsigned count    = 0;

    if (obj.m_currentSize == 0) return r;
    if (m_currentSize == 0)     return r;

    while (j < obj.m_currentSize) {
        if (position < 0) break;

        tmp.m_number[j] = m_number[position];
        tmp.m_currentSize += 1;
        position -= 1;
        j += 1;
    }

    tmp.reflect();

    if (tmp.isAbs(obj)) {
        if (position >= 0) {
            tmp.reflect();
            tmp.m_number[tmp.m_currentSize] = m_number[position];
            tmp.m_currentSize += 1;
            position -= 1;
            tmp.reflect();
        }
    }

    part = tmp;

    while (!part.isAbs(obj)) {
        part = part + -obj;
        count += 1;
    }

    r.m_number[i] = count;
    r.m_currentSize += 1;
    i += 1;

    while (position >= 0) {
        tmp = 0;
        tmp.m_number[0] = m_number[position];
        tmp.m_currentSize += 1;
        position -= 1;
        part.concat(tmp);
        tmp.remove_space();
        part.remove_space();
        tmp = part;

        while (tmp.isAbs(obj)) {
            if (position < 0) break;

            tmp.reflect();
            tmp.m_number[tmp.m_currentSize] = m_number[position];
            tmp.m_currentSize += 1;
            position -= 1;
            tmp.reflect();
            r.m_number[i] = 0;
            r.m_currentSize += 1;
            i += 1;
        }

        tmp.remove_space();
        part = tmp;
        count = 0;

        while (!part.isAbs(obj)) {
            part = part + -obj;
            count += 1;
        }

        r.m_number[i] = count;
        r.m_currentSize += 1;
        i += 1;
    }

    r.reflect();
    r.remove_space();

    reminder = part;
    return r;
}

void alon::init()
{
    m_number = new short [m_maxSize];

    for (unsigned i = 0; i < m_maxSize; i += 1)
        m_number[i] = 0;
}

void alon::init(const short *number, const unsigned size)
{
    m_number = new short [m_maxSize];

    for (unsigned i = 0; i < size; i += 1)
        m_number[i] = number[i];
}

void alon::reflect()
{
    if (m_currentSize > 1) {
        short *tmp = new short[m_currentSize];

        for (unsigned i = 0; i < m_currentSize; i += 1)
            tmp[i] = m_number[i];

        unsigned j = 0;
        for (int i = m_currentSize - 1; i >= 0; i -= 1) {
            m_number[j] = tmp[i];
            j += 1;
        }

        delete [] tmp;
    }
}

void alon::remove_space()
{
    unsigned deletePos = m_currentSize;

    for (unsigned i = 0; i < m_currentSize; i += 1) {
        if (m_number[i] == 0) {
            if (deletePos == m_currentSize)
                deletePos = i;
        } else {
            deletePos = m_currentSize;
        }
    }

    m_currentSize = deletePos;
}

bool alon::isAbs(const alon &obj)
{
    if (obj.m_currentSize > m_currentSize) return true;     else
    if (obj.m_currentSize < m_currentSize) return false;

    for (int i = m_currentSize - 1; i >= 0; i -= 1) {
        if (obj.m_number[i] > m_number[i]) return true;     else
        if (obj.m_number[i] < m_number[i]) return false;
    }

    return false;
}

alon operator -(const alon &obj)
{
    alon r(obj);
    r.m_positive = !r.m_positive;
    return r;
}

alon operator +(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    alon r;     r   = tmp + right;
    return r;
}

alon operator -(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    alon r;     r   = tmp - right;
    return r;
}

alon operator *(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    alon r;     r   = tmp * right;
    return r;
}

alon operator /(const int left, const alon &right)
{
    if (right.m_currentSize == 0)
        throw math_err("devision by zero");

    alon tmp;   tmp = left;
    alon r;     r   = tmp / right;
    return r;
}

alon operator %(const int left, const alon &right)
{
    if (right.m_currentSize == 0)
        throw math_err("reminder of devision by zero");

    alon tmp;   tmp = left;
    alon r;     r   = tmp % right;
    return r;
}

bool operator <(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return (tmp < right);
}

bool operator >(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return (tmp > right);
}

bool operator ==(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return (tmp == right);
}

bool operator !=(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return !(tmp == right);
}

bool operator <=(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return (tmp < right) || (tmp == right);
}

bool operator >=(const int left, const alon &right)
{
    alon tmp;   tmp = left;
    return (tmp > right) || (tmp == right);
}

std::ostream &operator <<(std::ostream &out, const alon &obj)
{
    if (!obj.m_positive)
        out << "-";

    for (int i = obj.m_currentSize - 1; i >= 0; i -= 1)
        out << obj.m_number[i];

    if (obj.m_currentSize == 0)
        out << 0;

    return out;
}

std::istream &operator >>(std::istream &in, alon &obj)
{
    char buf;
    obj.m_positive = true;
    obj.m_currentSize = 0;

    for (unsigned i = 0; i < obj.m_maxSize; ) {
        buf = in.get();

        if (buf == ' ' || buf == '\n' || buf == in.eof() )
            break;

        if (buf == '-')
            obj.m_positive = false;

        if (buf != '0' || buf != '1' || buf != '2' || buf != '3'
         || buf != '4' || buf != '5' || buf != '6' || buf != '7'
         || buf != '8' || buf != '9' ) {

            std::stringstream ss;
            ss << buf;
            ss >> obj.m_number[i];
            obj.m_currentSize += 1;
            i += 1;
        } else throw math_err("input data format is incorrect");
    }

    obj.reflect();
    obj.remove_space();
    return in;
}
