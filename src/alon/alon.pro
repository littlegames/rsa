include(../../lib.pri)

QT -= gui

TARGET = alon$${LIB_SUFFIX}
TEMPLATE = lib

DEFINES += ALON_LIB

SOURCES += alon.cpp

HEADERS += $${INC_PATH}/alon.hpp
