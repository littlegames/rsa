#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

#include <rsa.hpp>
#include <crypt_key.hpp>

//#include

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    flag = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_encrypt_clicked()
{
    if (!flag) {
        QMessageBox::warning(this, "Warning", "Create a folder");
        return;
    }

    QFile input(path + "/input");
    QFile output(path + "/output");
    QFile pub_key(path + "/pub_key");
    crypt_key key;

    if (input.open(QIODevice::ReadOnly)) {
        QTextStream fout(&input);
        ui->pln_input->setPlainText(fout.readAll());
    }

    int after, befor;

    if (pub_key.open(QIODevice::ReadOnly))
        key.load(pub_key.fileName().toLocal8Bit().constData());

    rsa::save(input.fileName().toLocal8Bit().constData()
            , output.fileName().toLocal8Bit().constData()
            , key);

    if (output.open(QIODevice::ReadOnly)) {
        QTextStream fout(&output);
        ui->pln_output->setPlainText(fout.readAll());
    }
}

void MainWindow::on_btn_decrypt_clicked()
{
    if (!flag)  {
        QMessageBox::warning(this, "Warning", "Create a folder");
        return;
    }

    QFile input(path + "/output");
    QFile output(path + "/decod");
    QFile prv_key(path + "/prv_key");
    crypt_key key;

    if (input.open(QIODevice::ReadOnly)) {
        QTextStream fout(&input);
        ui->pln_input->setPlainText(fout.readAll());
    }

    if (prv_key.open(QIODevice::ReadOnly))
        key.load(prv_key.fileName().toLocal8Bit().constData());

    rsa::load(input.fileName().toLocal8Bit().constData()
            , output.fileName().toLocal8Bit().constData()
            , key);

    if (output.open(QIODevice::ReadOnly)) {
        QTextStream fout(&output);
        ui->pln_output->setPlainText(fout.readAll());
    }
}

void MainWindow::on_btn_generate_clicked()
{
    if (!flag) {
        QMessageBox::warning(this, "Warning", "Create a folder");
        return;
    }

    QFile pub_key(path + "/pub_key");
    QFile prv_key(path + "/prv_key");
    QTextStream fout;

    crypt_key pub;
    crypt_key prv;

    rsa::gen_keys(pub, prv);

    pub.save(pub_key.fileName().toLocal8Bit().constData());
    prv.save(prv_key.fileName().toLocal8Bit().constData());

    if (pub_key.open(QIODevice::ReadOnly)) {
        fout.setDevice(&pub_key);
        ui->lnt_pubKey->setText(fout.readAll());
    }

    if (prv_key.open(QIODevice::ReadOnly)) {
        fout.setDevice(&prv_key);
        ui->lnt_prvKey->setText(fout.readAll());
    }
}

void MainWindow::on_actionNew_triggered()
{
    path = QFileDialog::getExistingDirectory(this, "Open directory", "./"
                                           , QFileDialog::ShowDirsOnly);

    QFile file;

    file.setFileName(path + "/input");
    file.open(QIODevice::ReadWrite);
    file.close();

    file.setFileName(path + "/output");
    file.open(QIODevice::ReadWrite);
    file.close();

    file.setFileName(path + "/decod");
    file.open(QIODevice::ReadWrite);
    file.close();

    file.setFileName(path + "/pub_key");
    file.open(QIODevice::ReadWrite);
    file.close();

    file.setFileName(path + "/prv_key");
    file.open(QIODevice::ReadWrite);
    file.close();

    flag = true;
}

void MainWindow::on_actionSave_triggered()
{
    if (!flag) {
        QMessageBox::warning(this, "Warning", "Create a folder");
        return;
    }

    QFile input(path + "/input");
    if (input.open(QIODevice::WriteOnly)) {
        QTextStream fin(&input);
        fin << ui->pln_input->toPlainText();
    }
}
