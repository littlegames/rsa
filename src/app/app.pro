include(../../app.pri)

QT  += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RSA
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS += $${INC_PATH}/mainwindow.h

FORMS   += mainwindow.ui

LIBS += -lalon$${LIB_SUFFIX}
LIBS += -L$${LIBS_PATH}/

LIBS += -lLibRSA$${LIB_SUFFIX}
LIBS += -L$${LIBS_PATH}/
