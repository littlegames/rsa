#include <rsa.hpp>

#include <cmath>
#include <sstream>

#include <alon.hpp>
#include <crypt_key.hpp>

constexpr unsigned short KEYLENGTH = 44;

class rsa_err {
    const char *m_what;
public:
    const char *what() { return m_what; }
    rsa_err(const char *input) : m_what (input) {}
};

rsa::rsa() {}

void rsa::gen_keys(crypt_key &pub, crypt_key &prv)
{
    alon p(KEYLENGTH / 2);
    alon q(KEYLENGTH / 2);
    alon d(KEYLENGTH - 2);
    alon n, eiler, divisor, e;

    while (!p.prime())  p.random(KEYLENGTH / 2);
    while (!q.prime())  q.random(KEYLENGTH / 2);

    n     = p * q;
    eiler = (p - 1) * (q - 1);  // Eiler's function

    do {    // Getting private & public exponents
        do {
            d.random(KEYLENGTH - 3);
            divisor = d.max_divisor(eiler);
        } while (divisor != 1);
        euclid(eiler, d, e);
    } while (e < 0);

    pub.a = d;
    pub.b = n;
    prv.a = e;
    prv.b = n;
}

void rsa::save(const char *in, const char *out, crypt_key &key)
{
    std::string buf;
    std::vector<char> a = key.a.toBinary();

    std::ifstream fin(in);
    if (!fin.good())    throw rsa_err("unable to open input file");

    std::ofstream fout(out);
    if (!fout.good())   throw rsa_err("unable to create output file");

    while (!fin.eof()) {
        buf.clear();

        for (unsigned i = 0; i < KEYLENGTH / 4; i += 1) {
            char bf = fin.get();
            if (fin.eof())  break;
            buf += bf;
        }

        if (!buf.empty())
            fout << encrypt(buf, a, key.b) << " ";
    }

    fin.close();
    fout.close();
}

void rsa::load(const char *in, const char *out, crypt_key &key)
{
    alon buf(KEYLENGTH * 2);
    std::vector<char> a = key.a.toBinary();

    std::ifstream fin(in);
    if (!fin.good())    throw rsa_err("unable to open input file");

    std::ofstream fout(out);
    if (!fout.good())   throw rsa_err("unable to create output file");

    while (true) {
        fin >> buf;
        if (fin.eof()) break;
        fout << decrypt(buf, a, key.b);
    }

    fin.close();
    fout.close();
}

alon rsa::encrypt(const std::string &data
                , const std::vector<char> &a
                , const alon &b)
{
    alon block; block = 1;

    if (data.size() > (KEYLENGTH / 4))
        throw rsa_err("size of the input string is exceeds side limit");

    for (unsigned i = 0; i < data.size(); i += 1) {
        alon buf;   buf = int(data[i]);

        if (buf < 0)
            buf += 256;

        block = block * int(std::pow(10.0, 3 - buf.size() ) );
        block.concat(buf);
    }

    return block.pow_mod(a, b);
}

std::string rsa::decrypt(alon &data
                       , const std::vector<char> &a
                       , const alon &b)
{
    std::stringstream ss;
    std::string buf;
    std::string r;
    int bf;
    alon tmp;
    alon block; block = data.pow_mod(a, b);

    while (block.size() > 3) {  // alon > string
        tmp   = block;
        block = block / 1000;

        ss.clear();
        ss << tmp - block * 1000;
        ss >> bf;

        if (bf > 127)
            bf -= 256;

        buf += char(bf);
    }

    if (block != 1)
        throw rsa_err("input data corrupted");

    for (int i = buf.size() - 1; i >= 0; i -= 1)
        r += buf[i];

    return r;
}

void rsa::euclid(alon a, alon b, alon &y)
{
    alon r, q;
    alon a11;   a11 = 1;
    alon a12;   a12 = 0;
    alon a21;   a21 = 0;
    alon a22;   a22 = 1;
    alon A11, A12, A21, A22;

    while (b > 0) {
        r = a % b;
        q = a / b;

        if (r == 0) break;

        A11 = a12;
        A12 = a11 + a12 * -q;
        A21 = a22;
        A22 = a21 + a22 * -q;

        a11 = A11;  a12 = A12; a21 = A21; a22 = A22;

        a = b;
        b = r;
    }

    y = a22;
}
