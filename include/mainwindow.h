#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

struct crypt_key;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_encrypt_clicked();
    void on_btn_decrypt_clicked();
    void on_btn_generate_clicked();
    void on_actionNew_triggered();
    void on_actionSave_triggered();

private:
    Ui::MainWindow *ui;
    QString path;
    bool flag;
};

#endif // MAINWINDOW_H
