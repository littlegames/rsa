#ifndef RSA_H
#define RSA_H

#include <string>
#include <vector>

class alon;
struct crypt_key;

class rsa
{
public:
    rsa();

public:
    /*!
     * \brief   gen_keys
     * \param   pub   public key
     * \param   prv   private key
     * \return  generating keys
     */
    static void gen_keys(crypt_key &pub, crypt_key &prv);

    /*!
     * \brief save
     * \param in
     * \param out
     * \param key
     */
    static void save(const char *in, const char *out, crypt_key &key);

    /*!
     * \brief load
     * \param in
     * \param out
     * \param key
     */
    static void load(const char *in, const char *out, crypt_key &key);

private:
    /*!
     * \brief   encrypt
     * \param   data
     * \param   a
     * \param   b
     * \return
     */
    static alon encrypt(const std::string &data
                      , const std::vector<char> &a
                      , const alon &b);

    /*!
     * \brief decrypt
     * \param data
     * \param a
     * \param b
     * \return
     */
    static std::string decrypt(alon &data
                      , const std::vector<char> &a
                      , const alon &b);

    /*!
     * \brief   euclid
     * \details Extended Euclodean algoroithn
     * \param   a
     * \param   b
     * \param   y   private exponet
     */
    static void euclid(alon a, alon b, alon &y);
};

#endif // RSA_H

