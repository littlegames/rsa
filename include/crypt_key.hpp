#ifndef CRYPT_KEY_HPP
#define CRYPT_KEY_HPP

#include <fstream>
#include <alon.hpp>

struct crypt_key {
    alon a;
    alon b;

    bool load(const char *file)
    {
        alon tmp(800);

        std::ifstream fin(file);

        if (fin.good()) {
            fin >> tmp;
            a = tmp;
            fin >> tmp;
            b = tmp;
            fin.close();
            return true;
        }

        return false;
    }


    bool save(const char *file)
    {
        std::ofstream fout(file);

        if (fout.good())
        {
            fout << a << std::endl;
            fout << b << std::endl;
            fout.close();
            return true;
        }

        return false;
    }
};

#endif // CRYPT_KEY_HPP
