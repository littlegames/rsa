/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew;
    QAction *actionSave;
    QAction *actionSave_As;
    QAction *actionOpen;
    QAction *actionClose;
    QAction *actionAbout;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLineEdit *lnt_pubKey;
    QLineEdit *lnt_prvKey;
    QPushButton *btn_generate;
    QGridLayout *gridLayout;
    QPlainTextEdit *pln_input;
    QPlainTextEdit *pln_output;
    QLabel *lbl_output;
    QLabel *lbl_input;
    QPushButton *btn_encrypt;
    QPushButton *btn_decrypt;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuHelp;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1032, 405);
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave_As = new QAction(MainWindow);
        actionSave_As->setObjectName(QStringLiteral("actionSave_As"));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lnt_pubKey = new QLineEdit(centralwidget);
        lnt_pubKey->setObjectName(QStringLiteral("lnt_pubKey"));
        lnt_pubKey->setClearButtonEnabled(true);

        verticalLayout->addWidget(lnt_pubKey);

        lnt_prvKey = new QLineEdit(centralwidget);
        lnt_prvKey->setObjectName(QStringLiteral("lnt_prvKey"));
        lnt_prvKey->setClearButtonEnabled(true);

        verticalLayout->addWidget(lnt_prvKey);

        btn_generate = new QPushButton(centralwidget);
        btn_generate->setObjectName(QStringLiteral("btn_generate"));

        verticalLayout->addWidget(btn_generate);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pln_input = new QPlainTextEdit(centralwidget);
        pln_input->setObjectName(QStringLiteral("pln_input"));

        gridLayout->addWidget(pln_input, 1, 1, 1, 1);

        pln_output = new QPlainTextEdit(centralwidget);
        pln_output->setObjectName(QStringLiteral("pln_output"));

        gridLayout->addWidget(pln_output, 1, 2, 1, 1);

        lbl_output = new QLabel(centralwidget);
        lbl_output->setObjectName(QStringLiteral("lbl_output"));
        lbl_output->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(lbl_output, 0, 2, 1, 1);

        lbl_input = new QLabel(centralwidget);
        lbl_input->setObjectName(QStringLiteral("lbl_input"));
        lbl_input->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(lbl_input, 0, 1, 1, 1);

        btn_encrypt = new QPushButton(centralwidget);
        btn_encrypt->setObjectName(QStringLiteral("btn_encrypt"));

        gridLayout->addWidget(btn_encrypt, 2, 1, 1, 1);

        btn_decrypt = new QPushButton(centralwidget);
        btn_decrypt->setObjectName(QStringLiteral("btn_decrypt"));

        gridLayout->addWidget(btn_decrypt, 2, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1032, 23));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menubar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);
        QWidget::setTabOrder(pln_output, lnt_prvKey);
        QWidget::setTabOrder(lnt_prvKey, btn_encrypt);
        QWidget::setTabOrder(btn_encrypt, pln_input);
        QWidget::setTabOrder(pln_input, btn_decrypt);
        QWidget::setTabOrder(btn_decrypt, btn_generate);
        QWidget::setTabOrder(btn_generate, lnt_pubKey);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuEdit->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_As);
        menuFile->addSeparator();
        menuFile->addAction(actionClose);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionNew->setText(QApplication::translate("MainWindow", "New", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MainWindow", "Save", Q_NULLPTR));
        actionSave_As->setText(QApplication::translate("MainWindow", "Save As...", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MainWindow", "Open...", Q_NULLPTR));
        actionClose->setText(QApplication::translate("MainWindow", "Close", Q_NULLPTR));
        actionAbout->setText(QApplication::translate("MainWindow", "About", Q_NULLPTR));
        lnt_pubKey->setPlaceholderText(QApplication::translate("MainWindow", "public key", Q_NULLPTR));
        lnt_prvKey->setPlaceholderText(QApplication::translate("MainWindow", "private key", Q_NULLPTR));
        btn_generate->setText(QApplication::translate("MainWindow", "generate", Q_NULLPTR));
        lbl_output->setText(QApplication::translate("MainWindow", "output", Q_NULLPTR));
        lbl_input->setText(QApplication::translate("MainWindow", "input", Q_NULLPTR));
        btn_encrypt->setText(QApplication::translate("MainWindow", "encrypt", Q_NULLPTR));
        btn_decrypt->setText(QApplication::translate("MainWindow", "decrypt", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
        menuEdit->setTitle(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
